# Adidas 2-step Custom-Activity Front End Assignment

## Information

This application has two branches:

* A JQuery branch (`master`): My initial take on the assignment
  * This branch is deployed in heroku and can be accessed through this link: [https://daverivera-adidas-salesforce.herokuapp.com/](https://daverivera-adidas-salesforce.herokuapp.com/)
* A React branch (`react`): My second take on the assignment using **React**
  * This branch is deployed in heroku and can be accessed through this link: [https://drivera-adidas-activity-react.herokuapp.com/](https://drivera-adidas-activity-react.herokuapp.com/)

Initially I created a branch using JQuery based on [Devs United](https://github.com/devsutd/journey-builder-activity-template)'s template, which is the `master` branch. Later I gave it another shot using **React**, which is the `react` branch.

## Running app locally

To run the app locally first install the dependencies with either `yarn` or `npm`

```
yarn install
```

After installing the dependencies, to run the application run the following command:

```
yarn start
```

The application will be accessible from the browser through: [http://localhost:3000](http://localhost:3000)


## Getting Started

### Configure web server
This guide covers Heroku, skip this step if you are familiar on how to deploy a Node.js app

1. Fork and Clone this repository
2. Login into [Heroku](https://heroku.com)
3. Click on New > Create new app
4. Give a name to the app and click on "Create App"
5. Choose your preferred Deployment method (Github or Heroku Cli are nice to work with)
6. Click on "Deploy branch"
7. Once your branch is deployed, click on the "View" button and verify you see the welcome message

#### Configure your package in Marketing Cloud

1. Login to Marketing Cloud and Navigate to Administration > Account > Installed Packages
2. Click on New and enter a name and a description for your package
3. **Copy the JWT Secret value from the Summary page and save it for later**
4. Click on Add Component, select Journey Builder Activity and Click next
5. Enter the information about the activity, enter [url of your activity] as your Endpoint URL
6. Click Save
7. **Copy the Unique Key value from the Journey Builder Activity panel and save it for later**

#### Configure Activity

Open /public/config.json and:
* Replace __application_key_from_appcenter_here__ for the value you got from step 7 from the previous section

#### Add Heroku vars

1. Log back into Heroku and navigate to your app
2. Click on "Settings"
3. Click on "Reveal config vars"
4. Add a new var called jwtSecret and paste the App Signature you got from step 3 when configuring your package in Marketing Cloud

#### Testing your Activity

1. Login into Marketing Cloud and navigate to Journey Builder
2. You should be able to see your custom activity and drag it into the canvas!
