define([
    'postmonger'
], function (
    Postmonger
) {
    'use strict';

    var connection = new Postmonger.Session();
    var authTokens = {};
    var payload = {};
    var step = 1;
    $(window).ready(onRender);

    connection.on('initActivity', initialize);
    connection.on('requestedTokens', onGetTokens);
    connection.on('requestedEndpoints', onGetEndpoints);

    connection.on('clickedNext', function () {
        step++;
        goToStep(step);
        connection.trigger('ready');
    });
    connection.on('clickedBack', function() {
        step--;
        goToStep(step);
        connection.trigger('ready');
    });

    connection.on('nextStep', function() {
        step = 2;
        goToStep(step);
        connection.trigger('ready');
    });


    function onRender() {
        $.ajax('https://mock-countries.herokuapp.com/list/', {
            success: function(countries) {
                countries.forEach(function(country) {
                    $('#selectCountry').append(new Option(country.name, country.code));
                });
            }
        });

        // JB will respond the first time 'ready' is called with 'initActivity'
        connection.trigger('ready');

        connection.trigger('requestTokens');
        connection.trigger('requestEndpoints');

    }

    function initialize(data) {
        console.log(data);
        if (data) {
            payload = data;
        }

        var hasInArguments = Boolean(
            payload['arguments'] &&
            payload['arguments'].execute &&
            payload['arguments'].execute.inArguments &&
            payload['arguments'].execute.inArguments.length > 0
        );

        var inArguments = hasInArguments ? payload['arguments'].execute.inArguments : {};

        console.log(inArguments);

        $.each(inArguments, function (index, inArgument) {
            $.each(inArgument, function (key, val) {


            });
        });

        goToStep(step);
    }

    function onGetTokens(tokens) {
        console.log(tokens);
        authTokens = tokens;
    }

    function onGetEndpoints(endpoints) {
        console.log(endpoints);
    }

    function save() {
        var notificationCountry = getCountry();
        var notificationMessage = getMessage();

        payload['arguments'].execute.inArguments.push({ country: notificationCountry.code });
        payload['arguments'].execute.inArguments.push({ message: notificationMessage });

        payload['metaData'].isConfigured = true;

        console.log(payload);
        connection.trigger('updateActivity', payload);
    }

    function getCountry() {
        var selectedCountry = $('#selectCountry').find('option:selected');

        return {
            // name: $('#selectCountry option:selected').text().trim(),
            name: selectedCountry.text().trim(),
            code: $('#selectCountry').find('option:selected').attr('value').trim()
        }
    }

    function getMessage() {
        return $('#push-notification-message').val().trim();
    }

    function goToStep(step) {
        $('.step').hide();
        switch(step) {
            case 1:
                $('#step1').show();
                connection.trigger('updateButton', { button: 'next', text: 'next', enabled: true });
                connection.trigger('updateButton', { button: 'back', visible: false });
                break;
            case 2:
                $('#step2').show();
                $('#showCountry').html(getCountry().name);
                $('#showMessage').html(getMessage());
                connection.trigger('updateButton', { button: 'back', visible: true });
                connection.trigger('updateButton', { button: 'next', text: 'done', visible: true });
                break;
            case 3: // Only 2 steps, so the equivalent of 'done' - send off the payload
                save();
                break;
        }
    }
});
